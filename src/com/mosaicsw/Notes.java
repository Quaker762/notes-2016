/*++

Module Name:
		Notes.java
		
Abstract:
		Program entry point.

Author:
        Jesse Buhagiar

Notes:

Revision History:
		22/9/2016: Initial Revision
		

--*/
package com.mosaicsw;

/**
 * @author Jesse
 *
 */
public class Notes
{
	private void init()
	{
		System.out.println("Hello World");
	}
	
	public static void main(String[] args)
	{
		Notes notes = new Notes();
		
		notes.init();
	}
}
