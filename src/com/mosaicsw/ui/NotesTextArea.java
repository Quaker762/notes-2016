/*++

Module Name:
		NotesTextArea.java

Abstract:
		Notes Custom text area. 

Author:
        Jesse Buhagiar

Notes:

Revision History:
		22/9/2016: Initial Revision

--*/
package com.mosaicsw.ui;

import javax.swing.JTextArea;

/**
 * @author Jesse
 *
 */
public class NotesTextArea extends JTextArea
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7143574660487134146L;
	
	

}
