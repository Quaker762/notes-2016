/*++

Module Name:
		NotesFrame.java
		
Abstract:
		Implementation of a JFrame that has a couple of custom functions. 
		
Author:
        Jesse Buhagiar

Notes:

Revision History:
		22/9/2016: Initial Revision
		

--*/
package com.mosaicsw.ui;

import javax.swing.JFrame;

/**
 * @author Jesse
 *
 */
public class NotesFrame extends JFrame
{
	private int 			width;
	private int 			height;
	private String			title;
	
	private NotesTextArea	textArea;
	
	public NotesFrame(int width, int height, String title)
	{
		this.width 	= width;
		this.height	= height;
		this.title 	= title;
	}
	
	
}
